﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WordCount
{
    public partial class MainWindow : Window
    {
        private List<WordCountClass> wordCountList = new List<WordCountClass>();
        private string path;
        private WordCountClass wordCountClass = new WordCountClass();
        private BackgroundWorker backgroundWorker = new BackgroundWorker();
        
        public MainWindow()
        {
            InitializeComponent();
            backgroundWorker = new BackgroundWorker();

            //add all necessary handlers to backgroundWorker
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;

            //let backgroundWorker listen to delegate
            wordCountClass.ReportProgressDelegate = backgroundWorker.ReportProgress;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();

            // Set filter for text files 
            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Filter = "TEXT Files (*.txt)|*.txt";

            // Display OpenFileDialog 
            Nullable<bool> result = openFileDialog.ShowDialog();

            // Get the  file name and display ít
            if (result == true)
            {
                path = textBox1.Text = openFileDialog.FileName;
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if(textBox1.Text != "noch keine Datei ausgewählt")
            {
                wordCountClass.cancelOperation = false;

                //Start the async operation
                backgroundWorker.RunWorkerAsync();

                //enable/disable buttons
                button.IsEnabled = false;
                button1.IsEnabled = false;
                button2.IsEnabled = true;

                Console.WriteLine("button1_Click");

                //clear the table
                listView.Items.Clear();
            }

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (backgroundWorker.IsBusy)
            {
                //Stop/Cancel the async operation
                backgroundWorker.CancelAsync();
                wordCountClass.cancelOperation = true;

                //enable button
                button.IsEnabled = true;
            }
        }

        void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //If it was cancelled midway
            if (e.Cancelled || wordCountClass.cancelOperation == true)
            {
                lblStatus.Content = "Aufgabe abgebrochen.";
            }
            else if (e.Error != null)
            {
                lblStatus.Content = "Fehler bei der Verarbeitung.";
            }
            else
            {
                lblStatus.Content = "Aufgabe erledigt...";
                Console.WriteLine("finished");
      
                //Add columns
                var gridView = new GridView();
                listView.View = gridView;

                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "Wort",
                    DisplayMemberBinding = new Binding("name")
                });

                gridView.Columns.Add(new GridViewColumn
                {
                    Header = "Anzahl",
                    DisplayMemberBinding = new Binding("count")
                });

                //add results to table
                foreach (var item in wordCountList)
                {
                    listView.Items.Add(new WordCountClass(item.name, item.count));
                }

            }

            //enable/disable buttons
            button.IsEnabled = true;
            button1.IsEnabled = true;
            button2.IsEnabled = false;
        }

        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //update the progressbar
            progressBar1.Value = e.ProgressPercentage;
            lblStatus.Content = "Verarbeite......" + progressBar1.Value.ToString() + "%";
        }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //long running task
            wordCountList = wordCountClass.DoTheCounting(path);
        }
    }
}
