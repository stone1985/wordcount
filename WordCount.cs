﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.ComponentModel;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Data;

namespace WordCount
{
    public class WordCountClass
    {
        //delegation for progress update
        public Action<int> ReportProgressDelegate { get; set; }
        public bool cancelOperation = false;

        public string name { get; set; }
        public int count { get; set; }

        public WordCountClass()
        {
        }

        public WordCountClass(string _name, int _count)
        {
            this.name = _name;
            this.count = _count;
        }

        //report value of progressbar
        private void ReportProgress(int percent)
        {
            if (ReportProgressDelegate != null)
                ReportProgressDelegate(percent);
        }

        
        public List<WordCountClass> DoTheCounting(string path)
        {
            int percentOld = 0, percentNew = 0;
            List<WordCountClass> wordCountList = new List<WordCountClass>();
            string[] separators = { " ", "\r\n", "\n", "\r" };
            double i = 0;

            //file will only be accessed if it exists
            if (File.Exists(path))
            {
                using (FileStream fileStream = File.Open(path, FileMode.Open, FileAccess.Read))
                using (StreamReader streamReader = new StreamReader(fileStream))
                {
                    //all words are written into a string array
                    string[] allWords = streamReader.ReadToEnd().Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    
                    //iterate over all words in string array
                    while (i < allWords.Length && cancelOperation == false)
                    {
                        //get word after word from string array
                        string word = allWords[(int)i]; 
                         
                        //calculate percent of done work     
                        percentNew = (int)(i / (allWords.Length - 1) * 100);

                        //only update progressbar if value changes. only integers are counted
                        if (percentNew > percentOld)
                        {
                            percentOld = percentNew;
                            ReportProgress(percentNew);
                            Console.WriteLine(percentNew);
                        }

                        //if word isn't allready in list, add a new one with count set to 1
                        if (wordCountList.FindIndex(item => item.name == word) == -1)
                        {
                            wordCountList.Add(new WordCountClass(word, 1));
                        }
                        //if word is allready in list, find it and increment count
                        else
                        {
                            wordCountList.Where(item => item.name == word).First().count++;
                        }
                        i++;
                    }
                }
            }
            else
            {
                cancelOperation = true;
            }
            return wordCountList;
        }
    }
}
